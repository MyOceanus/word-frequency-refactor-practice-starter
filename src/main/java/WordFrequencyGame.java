import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String CALCULATE_ERROR = "Calculate Error";

    public String getResult(String inputStr) {
        try {
            List<Words> wordsAndValue = getSplitWords(inputStr);
            List<Words> wordsValueAndCount = getInputValueAndCount(wordsAndValue);
            wordsValueAndCount.sort((firstWord, secondWord) -> secondWord.getWordCount() - firstWord.getWordCount());
            return getWordsWithFormat(wordsValueAndCount);
        } catch (Exception e) {
            return CALCULATE_ERROR;
        }
    }

    private static List<Words> getSplitWords(String inputStr) {
        String[] splitString = inputStr.split("\\s+");
        return Arrays.stream(splitString)
                .map(s -> new Words(s, 1))
                .collect(Collectors.toList());
    }

    private static String getWordsWithFormat(List<Words> wordsList) {
        return wordsList.stream()
                .map(words -> words.getValue() + " " + words.getWordCount())
                .collect(Collectors.joining("\n"));
    }

    private List<Words> getInputValueAndCount(List<Words> wordsList) {
        Map<String, List<Words>> stringListMap = groupByWord(wordsList);
        return stringListMap.entrySet().stream()
                .map(entry -> new Words(entry.getKey(),entry.getValue().size()))
                .collect(Collectors.toList());
    }

    private Map<String, List<Words>> groupByWord(List<Words> wordsList) {
        return wordsList.stream()
                .collect(Collectors.groupingBy(Words::getValue));

    }
}
