public class Words {
    private String value;
    private int count;

    public Words(String value, int count){
        this.value = value;
        this.count = count;
    }


    public String getValue() {
        return this.value;
    }

    public int getWordCount() {
        return this.count;
    }


}
